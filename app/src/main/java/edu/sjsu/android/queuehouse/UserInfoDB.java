package edu.sjsu.android.queuehouse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class UserInfoDB extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "userInfoDatabase";
    private static final int VERSION = 2;
    private static final String TABLE_NAME = "userInfo";
    protected static final String PHONE = "_phone";
    protected static final String EMAIL = "email";
    protected static final String PASSWORD = "password";
    protected static final String USERNAME = "name";
    protected static final String BALANCE = "balance";

    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " (" + PHONE + " TEXT PRIMARY KEY, "
                    + USERNAME + " TEXT,"
                    + PASSWORD + " TEXT NOT NULL,"
                    + BALANCE + " TEXT NOT NULL);";

    public UserInfoDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldV, int newV) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public long insert(ContentValues contentValues) {
        SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }


    public int update(ContentValues contentValues, String args[]){
        SQLiteDatabase database = getWritableDatabase();
        return database.update(TABLE_NAME, contentValues, "_phone =?", args);
    }

    public int delete() {
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_NAME, null, null);
    }

    public Cursor getAllUsers(String where, String args[]) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME,
                null, where, args, null, null, null);
    }

    public boolean isUserExisted(String phone, String password){
        SQLiteDatabase database = getWritableDatabase();
        String where = PHONE + " =? and " + PASSWORD + "=?";
        Cursor c = database.query(TABLE_NAME, null, where,
                new String[]{phone, password}, null, null, null);
        return c.moveToFirst();
    }
}
