package edu.sjsu.android.queuehouse;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import edu.sjsu.android.queuehouse.databinding.RowLayoutBinding;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>{
    ArrayList<Queuer> data;

    public HomeAdapter(ArrayList<Queuer> data){
        this.data = data;
    }

    @NonNull
    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // Inflate (= create) a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowLayoutBinding row = RowLayoutBinding.inflate(inflater);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Queuer current = data.get(position);
        holder.binding.name.setText(current.getName());
        holder.binding.image.setImageResource(current.getImage());
        holder.binding.place.setText(current.getPlace());
        holder.binding.rank.setText(current.getRank());
        holder.binding.price.setText(current.getPrice());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected final RowLayoutBinding binding;
        public ViewHolder(RowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
