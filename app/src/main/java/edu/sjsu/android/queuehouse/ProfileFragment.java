//Author:Xinken Zheng


package edu.sjsu.android.queuehouse;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;


public class ProfileFragment extends Fragment {
    private Uri uri = OrdersProvider.CONTENT_URI;
    private TextView userName;
    private String user;
    private TextView coins;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_profile, container, false);
        Button button = (Button) view.findViewById(R.id.queuer);
        String coinNum = "";
        coins =view.findViewById(R.id.coins);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), beQueuer.class);
                startActivity(intent);
            }
        });
        userName = view.findViewById(R.id.username);
        try (Cursor c = getActivity().getContentResolver().query(UserInfoProvider.CONTENT_URI, null, null, null, "name")) {
            if (c.moveToFirst()) {
                do {
                    if(c.getString(0).equals(SignInActivity.userPhone)){
                        user=c.getString(1);
                        coinNum=c.getString(3);
                    }
                } while (c.moveToNext());
            }
        }
        userName.setText(user);
        coins.setText(coinNum);
        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.overflow_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == -1){
            Log.d("CCCC", "1");
            coins.setText(data.getStringExtra("balance"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.purchase:
                Intent intent = new Intent(getActivity(), CoinPurchaseActivity.class);
                startActivityForResult(intent,1);
                return true;
            case R.id.deleteHistory:
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setMessage("Are you sure to delete all the order history?")
                        .setPositiveButton("No!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try (Cursor c = getActivity().getContentResolver().
                                        query(uri, null, null, null, "name")) {
                                    if (c.moveToFirst()) {
                                        do {
                                            getActivity().getContentResolver().delete(uri,c.getString(0),null);
                                        } while (c.moveToNext());
                                    }
                                }
                                Toast.makeText(getActivity(), "All the orders history deleted!!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();

                return true;
            case R.id.Uninstall:
                Intent delete = new Intent(Intent.ACTION_DELETE,
                        Uri.parse("package:" + getActivity().getPackageName()));
                startActivity(delete);
                return true;
            case R.id.loggout:
                Intent logout = new Intent(getActivity(), UIActivity.class);
                startActivity(logout);
                return true;
            case R.id.profile_pic:
                Toast.makeText(getActivity(), "Sorry, this feature is currently not available",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.change_pw:
                Toast.makeText(getActivity(), "Contact support to make a password change",Toast.LENGTH_SHORT).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}