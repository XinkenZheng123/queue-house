package edu.sjsu.android.queuehouse;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private RecyclerView recycler;
    RecyclerView.Adapter<HomeAdapter.ViewHolder> homeAdapter;
    ArrayList<Queuer> input = new ArrayList<>();
    private Uri uri = QueuersProvider.CONTENT_URI;
    public static int current;


  /*  // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    */

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        inflater.inflate(R.menu.delete,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //setHasOptionsMenu(true);
        input.add(new Queuer("Tony Xie",R.drawable.leagueoflegend,"Shanghai","League Of Legend: Challenger","99$/hr","Challenger is the highest rank in the game \"League of Legends\" qualifying. The ultimate goal pursued by all summoners is that the very few who can reach the Challenger are above Grand Master."));
        input.add(new Queuer("Xinkeng Zheng",R.drawable.ken,"San Jose","Apex Legends: Gold 4","5$/hr","A normal SJSU computer science student with normal Apex Legends skill."));
        input.add(new Queuer("Zehui Zhan",R.drawable.james,"Guangzhou","League Of Legend: Iron 5","150$/hr","The photo is a fake picture, please don't be fooled."));
        input.add(new Queuer("SKT T1 Faker",R.drawable.faker,"South Korea","League Of Legend: Challenger","100$/hr","Lee Sang-hyeok, better known as Faker, is a South Korean professional League of Legends player for T1. Formerly known as GoJeonPa on the Korean server, he was picked up by LCK team SK Telecom T1 in 2013 and has played as the team's mid laner since."));
        input.add(new Queuer("Simple",R.drawable.simple,"Ukraine","CSGO: The Global Elite","88$/hr","Oleksandr Kostyliev, better known as s1mple, is a Ukrainian professional Counter-Strike: Global Offensive player for Natus Vincere. He is considered to be one of the best players in Global Offensive history. "));
        input.add(new Queuer("UZI",R.drawable.uzi,"China","League Of Legend: Challenger","100$/hr","Jian Zihao, better known by his in-game name Uzi, is a Chinese retired professional League of Legends player. Widely regarded as the greatest AD carry of all time, he was renowned for his mechanical prowess on champions such as Vayne, Kai'Sa, Ezreal and Kog'Maw."));
        input.add(new Queuer("Doublelift",R.drawable.doublelift,"California","League Of Legend: Challenger","50$/hr","Yiliang \"Peter\" Peng, better known by his in-game name Doublelift, is an American content creator, streamer, and former professional League of Legends player. He previously played for Counter Logic Gaming, Team Liquid and Team SoloMid. "));
        try (Cursor c = getActivity().getContentResolver().query(uri, null, null, null, "name")) {
            if (c.moveToFirst()) {
                do {
                    Queuer item = new Queuer(c.getString(1),R.drawable.logo,c.getString(2),c.getString(3),c.getString(4)+"$/hr",c.getString(5));
                    input.add(item);
                } while (c.moveToNext());
            }
        }
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home,
                container, false);
        recycler = view.findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(view.getContext()));
        homeAdapter = new HomeAdapter(input);
        recycler.setAdapter(homeAdapter);
        recycler.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        current = position;
                        Intent intent = new Intent(view.getContext(), DetailActivity.class);
                        intent.putExtra("current", input.get(position));
                        startActivity(intent);
                    }
                }));
        return view;
    }
}