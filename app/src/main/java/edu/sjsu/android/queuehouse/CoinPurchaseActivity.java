//Author:Xinken Zheng


package edu.sjsu.android.queuehouse;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import edu.sjsu.android.queuehouse.databinding.ActivityMainBinding;


public class CoinPurchaseActivity extends Activity {
    private RadioButton five;
    private RadioButton ten;
    private RadioButton twenty;
    private RadioButton fifty;
    private RadioButton hundred;
    private TextView balance;
    private Button back;
    private final Uri uri = UserInfoProvider.CONTENT_URI;
    private String phone;
    private String text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coin_purchase);
        five = findViewById(R.id.five);
        ten = findViewById(R.id.ten);
        twenty = findViewById(R.id.twenty);
        fifty = findViewById(R.id.fifty);
        hundred = findViewById(R.id.hundred);
        back = findViewById(R.id.back);
        back.setOnClickListener(v -> {
            backToProfile();
        });
        balance = findViewById(R.id.balanceAmount);
        SharedPreferences sp = getSharedPreferences("userID", MODE_PRIVATE);
        phone = sp.getString("phone", null);
        Cursor c = getContentResolver().query(uri, null, "_phone=?", new String[]{phone}, null);
        if(c.moveToFirst()){
            text = c.getString(3);
        }
        balance.setText(text);
    }

    public void backToProfile() {
        Intent intent = new Intent();
        intent.putExtra("balance", text);
        setResult(-1, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        backToProfile();
    }

    public void onClick(View view){
        updateBalance();
    }

    public void updateBalance(){
            int amount = Integer.parseInt(text);
            if(five.isChecked()) {
                amount += 5;
            }
            else if(ten.isChecked()){
                amount  += 10;
            }
            else if(twenty.isChecked()){
                amount += 20;
            }
            else if(fifty.isChecked()) {
                amount += 50;
            }
            else if(hundred.isChecked()){
                amount  += 100;
            }
            balance.setText(String.valueOf(amount));
            text = String.valueOf(amount);
            ContentValues cv = new ContentValues();
            cv.put("balance", String.valueOf(amount));
            getContentResolver().update(uri, cv, null, new String[]{phone});
            Toast.makeText(this, "Transaction Successful: coins has been added to your balance",Toast.LENGTH_SHORT).show();
    }
}


