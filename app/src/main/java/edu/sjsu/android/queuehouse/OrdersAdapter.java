package edu.sjsu.android.queuehouse;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    ArrayList<Queuer> data;

    public OrdersAdapter(ArrayList<Queuer> data){
        this.data = data;
    }

    @NonNull
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // Inflate (= create) a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Queuer current = data.get(position);
        holder.orderPicure.setImageResource(current.getImage());
        holder.orderName.setText(current.getName());
        holder.hours.setText("Total Hours: "+current.getHours());
        holder.cost.setText("Cost: $"+current.getCost());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView orderPicure;
        public TextView orderName;
        public TextView hours;
        public TextView cost;

        public ViewHolder(View view) {
            super(view);
            orderPicure = view.findViewById(R.id.orderPicture);
            orderName = view.findViewById(R.id.orderName);
            hours = view.findViewById(R.id.hours);
            cost = view.findViewById(R.id.cost);
        }
    }
}
