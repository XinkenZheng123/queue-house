package edu.sjsu.android.queuehouse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class QueuersDB extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "queuersDatabase";
    private static final int VERSION = 1;
    private static final String TABLE_NAME = "queuers";
    private static final String ID = "_id";
    private static final String NAME = "name";
    private static final String PLACE = "place";
    private static final String RANK = "rank";
    private static final String PRICE = "price";
    private static final String DES = "des";

    static final String CREATE_TABLE =
            " CREATE TABLE " + TABLE_NAME +
                    " ("+ ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME + " TEXT NOT NULL, "
                    + PLACE + " TEXT NOT NULL, "
                    + RANK + " TEXT NOT NULL, "
                    + PRICE + " TEXT NOT NULL, "
                    + DES + " TEXT NOT NULL);";

    public QueuersDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long insert(ContentValues contentValues) { SQLiteDatabase database = getWritableDatabase();
        return database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllQueuers(String orderBy) {
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_NAME,
            new String[]{ID, NAME, PLACE,RANK,PRICE,DES},
            null, null, null, null, orderBy);
    }
}
